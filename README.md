## Introduction

This is the simulation system for development of high level strategies 
created by the brazilian RoboCup team Taura Bots. It works as a server/client
system where the server is where the perception and action is done and the 
client is where the mind of the agent is programmed, receiving data about the 
objects perceived in the world and sending commands to be persisted in the world
 by the body.

## Instalation
You will need *python 3.x*. On ubuntu simply use the command:

```
sudo apt-get install python3
```
The application uses the graphic library *pygame*. The problem is pygame doesn't
 work on *python 3* so you will have to download the source code of *pygame* and
 compile it with *python 3*

You can do it following the steps:

1. Go to your home directory
```
cd ~
```
2. Download *mercurial* to get the *pygame* source code
```
sudo apt-get install mercurial
```
3. Clone the source code and go inside it's directory
```
hg clone https://bitbucket.org/pygame/pygame
cd pygame
```
4. Install the dependencies to compile *pygame*
```
sudo apt-get install python3-dev python3-numpy libsdl-dev libsdl-image1.2-dev \
libsdl-mixer1.2-dev libsdl-ttf2.0-dev libsmpeg-dev libportmidi-dev \
libavformat-dev libswscale-dev libjpeg-dev libfreetype6-dev
```
(the three lines above are all one single command)
5. Build the project using *python 3*
```
python3 setup.py build
```
6. Install *pygame* using *python 3*
```
sudo python3 setup.py install
```
7. Test it
```
python3
>>> import pygame
```
If no error is raised, your installation of *pygame* with *python 3* worked! :)

## Usage
To simulate the world: 
```
python3 WorldSimulator.py
```
To connect a mind to a robot (this example uses index 0):
```
python3 RobotSimulator.py 0
```
Using `python3 RobotSimulator.py -h` will give you help on other ways to use the
 robot interface.

--------------------------------------------------------------------------------

# Documentation on the robot interface
## Robot/Controller.py
### class Controller
This is the interface to control the robot.

##### Member attributes:

- `graphic_mode: bool`
- `view: Robot.RobotView`
- `world: Robot.R_WorldModel`
- `index: int`
- `message: dict`
- `communicator: Robot.Communicator`

##### Methods defined here:
    
**`__init__(index)`**

Initializes the graphic interface if set, creates `world` object that 
stores a list of objects detected on the world, stores the index of 
which robot it should communicate to and initializes the JSON message.
    
**`createWorldFromDict(wdict)`**

Receives a python dictionary and creates a list of objects
    
**`getWorld()`**

Returns the world object, which contain the list of objects detected
on the world.
    
**`setHeadAngle(head_angle)`**
    
**`setKick(kick)`**

Sets the kick command being 0 no kick, 1 left leg kick and -1 right
leg kick
    
**`setMovementVector(movement_vector)`**

Sets the movement vector that makes the robot move. The format is 
Vector2(r, a, phi)
    
**`updateSimulation()`**

Refreshes the screen if graphic mode is set and comunicates with the 
robot sending commands and getting information about the world.

## Robot/Model.py

### class R_BaseObjectModel(\_\_builtin\_\_.object)
    
This class defines what attributes a basic object should have. This is 
the objects the robot can detect.
    
##### Member attributes:

- `position: (float, float)`
- `kind: str`

##### Methods defined here:
    
**`__init__(pos, kind='unkown')`**

Initializes the object with a position and a kind.
Position is a tuple of (r,a).
Kind is one of the strings ["ball", "pole", "robot", "unkown"].

**`getJSON()`**

Returns the JSON object of this object.

### class R_WorldModel
This class stores a list of objects detected by the robot. This list is 
refreshed everytime the simulation is updated (everytime the robot receives 
information about the world)

##### Member attributes:

- `objects_list: list`

##### Methods defined here:

**`__init__()`**

Initializes the `objects_list` attribute as an empty list

**`getDict()`**

Returns the list as a python dictionary

**`getJSON()`**

Returns the JSON object of this object

## Robot/Communicator.py

### class Communicator
This class makes the communication via UDP sockets between the agent and 
the robot it controls.

##### Methods defined here:
- `address`
- `talk_sock`
- `listen_sock`
- `index`
- `talking_port`
- `listening_port`

**`__init__(index=0)`**

Initializes the address for communication, if no ip address is given
it defaults to localhost. Two socket objects are created, one for 
talking and one for listening aswell as the ports for talking and 
listening. Also, it sets the index of which robot it should communicate
to.

**`communicate(msg)`**

Talks and returns the message received as response.

**`createMessage(msg)`**

Receives a python dictionary and returns its equivalent as a JSON 
string

**`listen()`**

Tries to receive the message from the body, being this body simulated
or not. It returns the world as a dictionary.

**`talk(msg)`**

Tries to send the message from the agent to the body, being this
body simulated or not.